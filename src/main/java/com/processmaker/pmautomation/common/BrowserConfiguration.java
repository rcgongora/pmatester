package com.processmaker.pmautomation.common;

import java.io.IOException;

public class BrowserConfiguration {
    private static BrowserConfiguration ourInstance = new BrowserConfiguration();

    /**
     * Get the current Browser Configuration
     * @return Current BrowserConfiguration
     */
    public static BrowserConfiguration getInstance() {
        return ourInstance;
    }

    /**
     * Constructor that set parameters to current browser.
     */
    private BrowserConfiguration() {
        //init browser configuration by default
        try {
            getBrowserMode();
            getBrowserName();
            getBrowserVersion();
            getBrowserPlatform();
            getRemoteServerUrl();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    /**
     * Get the Browser Mode
     * @return "local" or "remote"
     * @throws IOException
     */
    public String getBrowserMode() throws IOException {
        //read configuration from registry if not found read from configuration file and store in registry
        String browserMode = (String)Registry.getInstance().getReference("browser.mode");
        if(browserMode == null){
            browserMode = ConfigurationSettings.getInstance().getSetting("browser.mode");
            Registry.getInstance().register("browser.mode", browserMode);
        }

        return browserMode;
    }

    /**
     * Set Browser Mode
     * @param browserMode String with "local" or "remote"
     * @throws IOException
     */
    public void setBrowserMode(String browserMode) throws IOException {
        //set configuration in registry
        Registry.getInstance().register("browser.mode", browserMode);
    }

    /**
     * get the browser on which the test was executed
     * @return "Firefox", "Chrome", "IE"
     * @throws IOException
     */
    public String getBrowserName() throws IOException {
        String browserName = (String)Registry.getInstance().getReference("browser.name");
        if(browserName == null){
            browserName = ConfigurationSettings.getInstance().getSetting("browser.name");
            Registry.getInstance().register("browser.name", browserName);
        }

        return browserName;
    }

    /**
     * Set the browser on which the test was executed
     * @param browserName String with Browser name ("firefox","chrome","ie")
     * @throws IOException
     */
    public void setBrowserName(String browserName) throws IOException {
        //set configuration in registry
        Registry.getInstance().register("browser.name", browserName);
    }

    /**
     * Get Browser version in the which run the test.
     * @return Browser version
     * @throws IOException
     */
    public String getBrowserVersion() throws IOException {
        String browserVersion = (String)Registry.getInstance().getReference("browser.version");
        if(browserVersion == null){
            browserVersion = ConfigurationSettings.getInstance().getSetting("browser.version");
            Registry.getInstance().register("browser.version", browserVersion);
        }

        return browserVersion;
    }

    /**
     * Set Browser Version
     * @param browserVersion Browser Version
     * @throws IOException
     */
    public void setBrowserVersion(String browserVersion) throws IOException {
        //set configuration in registry
        Registry.getInstance().register("browser.version", browserVersion);
    }

    /**
     * Get the operating system
     * @return "Windows" or "Linux"
     * @throws IOException
     */
    public String getBrowserPlatform() throws IOException {
        String browserPlatform = (String)Registry.getInstance().getReference("browser.platform");
        if(browserPlatform == null){
            browserPlatform = ConfigurationSettings.getInstance().getSetting("browser.platform");
            Registry.getInstance().register("browser.platform", browserPlatform);
        }
        return browserPlatform;
    }

    /**
     * Set the operating system
     * @param browserPlatform String with "WINDOWS" or "LINUX"
     * @throws IOException
     */
    public void setBrowserPlatform(String browserPlatform) throws IOException {
        //set configuration in registry
        Registry.getInstance().register("browser.platform", browserPlatform);
    }

    /**
     * Get url of Selenium server to remote testing
     * @return url of the hub (http://192.168.11.169:4444/wd/hub)
     * @throws IOException
     */
    public String getRemoteServerUrl() throws IOException {
        String remoteServerUrl = (String)Registry.getInstance().getReference("remote.server.url");
        if(remoteServerUrl == null){
            remoteServerUrl = ConfigurationSettings.getInstance().getSetting("remote.server.url");
            Registry.getInstance().register("remote.server.url", remoteServerUrl);
        }

        return remoteServerUrl;
    }

    /**
     * Set the url of Selenium Server for remote test
     * @param remoteServerUrl url with the format http://192.168.11.169:4444/wd/hub
     * @throws IOException
     */
    public void setRemoteServerUrl(String remoteServerUrl) throws IOException {
        //set configuration in registry
        Registry.getInstance().register("remote.server.url", remoteServerUrl);
    }
}
