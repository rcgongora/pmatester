package com.processmaker.pmautomation.common;

public enum PMBPMNElementCoordinates {
    NORTH,
    SOUTH,
    EAST,
    WEST,
    NORTHWEST,
    NORTHEAST,
    SOUTHWEST,
    SOUTHEAST
}
