package com.processmaker.pmautomation.common;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Utils {
    /**
     * Gets a random string containing the characters included from 0 to 9 and A to Z
     * @param longString length of the string
     * @return Random string of the length n
     */
    public static String getRandomString (int longString){
        String randomString = "";
        long milis = new java.util.GregorianCalendar().getTimeInMillis();
        Random r = new Random(milis);
        int i = 0;
        while ( i < longString){
            char c = (char)r.nextInt(255);
            if ( (c >= '0' && c <='9') || (c >='A' && c <='Z') ){
                randomString += c;
                i ++;
            }
        }
        return randomString;
    }

    /**
     * Get the path of directory of download
     * @param browserInstance
     * @return path of directory of donwloads
     * @throws IOException
     */
    public static String getDownloadWorkingDirectory(BrowserInstance browserInstance) throws IOException {
        String workingPath = "";
        workingPath = getWorkingDirectory(browserInstance);
        workingPath += getFileSeparator();
        workingPath += "downloads";
        return workingPath;
    }

    /**
     * Get the path of directory of work
     * @param browserInstance
     * @return directory path
     * @throws IOException
     */
    public static String getWorkingDirectory(BrowserInstance browserInstance) throws IOException {
        String workingPath = "";

        if(browserInstance.getBrowserPlatform().equals("WINDOWS")){
            workingPath = ConfigurationSettings.getInstance().getSetting("windows.working.directory");
        }
        if(browserInstance.getBrowserPlatform().equals("LINUX")){
            workingPath = ConfigurationSettings.getInstance().getSetting("linux.working.directory");
        }
        return workingPath;
    }

    /**
     * Get the file separator based in Operating System
     * @return file separator.
     */
    public static String getFileSeparator(){
        return System.getProperty("file.separator");
    }

    /**
     * Verify if the WebElement is stale
     * @param element Web Element to check
     * @return true if is a stale element.
     */
    public static boolean isStaleElement(WebElement element){
        boolean stale = true;
        try{
            element.getAttribute("id");
            stale = false;
        }catch(StaleElementReferenceException ex){
            stale = true;
        }

        return stale;
    }

    /**
     * Add working days to date
     * @param currentDay start date
     * @param workingDays number of day to add
     * @return date with working days add
     */
    public static Calendar addWorkingDays(Date currentDay, int workingDays) {
        Calendar startCal = Calendar.getInstance();
        startCal.setTime( currentDay );
        while (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || startCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
            startCal.add(Calendar.DAY_OF_MONTH, 1);
        }
        if (startCal.get(Calendar.HOUR_OF_DAY) < 9  ){
            startCal.add(Calendar.DAY_OF_MONTH, -1);
        }
        for(int i = workingDays; i > 0; i--) {
            startCal.add(Calendar.DAY_OF_MONTH, +1);
            while (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || startCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
            {
                startCal.add(Calendar.DAY_OF_MONTH, +1);
            }
        }
        return startCal;
    }

    /**
     * Add working hours to a date
     * @param currentDay start date
     * @param workingHours hours for add
     * @return date with hours add
     */
    public static Calendar addWorkingHours(Date currentDay, int workingHours) {
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(currentDay);
        while (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || startCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
            startCal.add(Calendar.DAY_OF_MONTH, 1);
        }
        for(int i = workingHours; i > 0; i--){
            startCal.add(Calendar.HOUR_OF_DAY, +1);
            while (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || startCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY){
                startCal.add(Calendar.DAY_OF_MONTH, +1);
            }
            if (startCal.get(Calendar.HOUR_OF_DAY) >17){
                startCal.set(startCal.get(Calendar.YEAR),startCal.get(Calendar.MONTH),startCal.get(Calendar.DAY_OF_MONTH)+1,9,startCal.get(Calendar.MINUTE),0);
                i++;
            }
            if (startCal.get(Calendar.HOUR_OF_DAY) < 9  ){
                startCal.set(startCal.get(Calendar.YEAR),startCal.get(Calendar.MONTH),startCal.get(Calendar.DAY_OF_MONTH),9,startCal.get(Calendar.MINUTE),0);
                i++;
            }
        }
        if(startCal.get(Calendar.MINUTE) > 0 &&startCal.get(Calendar.HOUR_OF_DAY) ==17){
            startCal.set(startCal.get(Calendar.YEAR),startCal.get(Calendar.MONTH),startCal.get(Calendar.DAY_OF_MONTH)+1,9,startCal.get(Calendar.MINUTE),0);

        }
        return startCal;
    }

    /**
     * Deletes a case of a specified workspace
     * @param workspace Workspace to delete case
     * @param numberCase Number case for delete
     * @param browserInstance Browser instance
     * @throws IOException
     */
    public static void deletedCaseFromWorkspace(String workspace,int numberCase,BrowserInstance browserInstance) throws IOException {
        String url = ConfigurationSettings.getInstance().getSetting("server.url");
        //SubString for index.html
        if(url.contains("index.html"))
            url = url.substring(0,21);
        browserInstance.gotoUrl(url + "/deleteCase.php?workspace=" + workspace + "&app_number=" + numberCase);
    }

    /**
     * Deletes all cases of a workspace
     * @param workspace Workspace to delete all cases
     * @param browserInstance Browser instance
     * @throws IOException
     */
    public static void deleteAllCases(String workspace,BrowserInstance browserInstance) throws IOException {
        String url = ConfigurationSettings.getInstance().getSetting("server.url");
        //SubString for index.html
        if(url.contains("index.html"))
            url = url.substring(0,21);
        browserInstance.gotoUrl(url + "/deleteAllCases.php?workspace=" + workspace);
    }

    /**
     * Make a call to "generateCases.php" script file and generates cases workspace
     * @param inbox Number of cases generated Inbox
     * @param draft Number of cases generated Draft
     * @param paused Number of cases generated Paused
     * @param workspace Name of the workspace
     * @param titleProcess Title Process
     * @param titleTask Title Task
     * @param browserInstance Browser instance
     * @throws IOException
     */
    public static void generateCases(int inbox, int draft, int paused, String workspace, String titleProcess, String titleTask,BrowserInstance browserInstance) throws IOException {
        String url = ConfigurationSettings.getInstance().getSetting("server.url");
        //SubString for index.html
        if(url.contains("index.html"))
            url = url.substring(0,21);
        browserInstance.gotoUrl(url + "/sys" + workspace + "/en/neoclassic/services/generateCases?inbox=" + inbox + "&draft=" + draft + "&paused=" + paused + "&process=" + titleProcess + "&task=" + titleTask);
    }
}
