package com.processmaker.pmautomation.common;

import java.io.IOException;

public class Logger {
    public Logger(){

    }

    public static void addLog(String logText){
        String debugEnabled = null;
        try{
            debugEnabled = ConfigurationSettings.getInstance().getSetting("debug.enable");
        } catch(IOException e) {
            e.printStackTrace();
        }
        if(debugEnabled.equals("1")){
            System.out.println(logText);
        }
    }
}
