package com.processmaker.pmautomation.common;

public class BrowserSettings {
    private String _browserMode;
    private String _browserName;
    private String _browserVersion;
    private String _browserPlatform;
    private String _remoteServerURL;

    /**
     * Get the Browser Mode (remote or local)
     * @return "remote" or "local"
     */
    public String getBrowserMode() {
        return _browserMode;
    }

    /**
     * Set Browser mode with "remote" or "local"
     * @param browserMode "remote" or "local"
     */
    public void setBrowserMode(String browserMode) {
        if(browserMode != null)
            _browserMode = browserMode;
    }

    /**
     * Get the Browser Name ("Firefox","Chrome","ie")
     * @return "Firefox", "Chrome", "ie"
     */
    public String getBrowserName() {
        return _browserName;
    }

    /**
     * Set Browser Name ("Firefox","Chrome","ie")
     * @param browserName "Firefox", "Chrome" or "ie"
     */
    public void setBrowserName(String browserName) {
        if(browserName != null)
            _browserName = browserName;
    }

    /**
     * Get Browser version in the which run the test
     * @return Browser version
     */
    public String getBrowserVersion() {
        return _browserVersion;
    }

    /**
     * Set Browser version in the which run the test
     * @param browserVersion browser version
     */
    public void setBrowserVersion(String browserVersion) {
        if(browserVersion != null)
            _browserVersion = browserVersion;
    }

    /**
     * Get the operating system
     * @return "Windows", "Linus", ...
     */
    public String getBrowserPlatform() {
        return _browserPlatform;
    }

    /**
     * Set the operating system for run test
     * @param browserPlatform "Windows", "Linux"
     */
    public void setBrowserPlatform(String browserPlatform) {
        if(browserPlatform != null)
            _browserPlatform = browserPlatform;
    }

    /**
     * Get the url to the Selenium Grid
     * @return http://192.168.11.169:4444/wd/hub
     */
    public String getRemoteServerUrl() {
        return _remoteServerURL;
    }

    /**
     * Set the url to the Selenium Grid
     * @param remoteServerUrl Url with the format http://192.168.11.169:4444/wd/hub
     */
    public void setRemoteServerUrl(String remoteServerUrl) {
        if(remoteServerUrl != null)
            _remoteServerURL = remoteServerUrl;
    }

    /**
     * Get the String that resume the configuration of browser.
     * @return String with all parameters of the configuration browser.
     */
    public String toString() {
        return "Browser Mode:" + _browserMode + ", Browser Name:" + _browserName +
                ", Browser Version:" + _browserVersion + ", Browser Platform:" + _browserPlatform +
                ", Remote Server URL:" + _remoteServerURL;
    }
}
