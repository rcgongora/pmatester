package com.processmaker.pmautomation.pages;

import com.processmaker.pmautomation.common.BrowserInstance;
import com.processmaker.pmautomation.common.ConfigurationSettings;
import com.processmaker.pmautomation.common.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class Login extends PMPage {
    WebElement user;
    WebElement password;
    WebElement submitButton;
    List<WebElement> auxListElements = null;

    public Login(BrowserInstance browserInstance) throws Exception {
        super(browserInstance);
        verifyPage();
    }

    @Override
    public void verifyPage() throws Exception {
        browser.switchToDefaultContent();

        auxListElements = webDriver.findElements(By.xpath("//input[@name='username']"));
        if(auxListElements.size() > 0){
            user = auxListElements.get(0);
            Logger.addLog("User Field successfully found");
        }else{
            throw new Exception("User Field not found");
        }

        auxListElements = webDriver.findElements(By.xpath("//input[@name='password']"));
        if(auxListElements.size() > 0){
            password = auxListElements.get(0);
            Logger.addLog("Password Field successfully found");
        }else{
            throw new Exception("Password Field not found");
        }

        auxListElements = webDriver.findElements(By.xpath("//button[text()='Login']"));
        if(auxListElements.size() > 0){
            submitButton = auxListElements.get(0);
            Logger.addLog("Login button successfully found");
        }else{
            throw new Exception("Login Button not found");
        }
    }

    public void loginFocos(String userName, String password) throws Exception{
        this.user.sendKeys(userName);
        this.password.sendKeys(password);
        submitButton.click();
    }
}