package com.processmaker.pmautomation.pages;

import com.processmaker.pmautomation.common.BrowserInstance;
import com.processmaker.pmautomation.common.ConfigurationSettings;
import org.openqa.selenium.WebDriver;

public abstract class Page {
    protected String url;
    protected String pageTitle;
    protected BrowserInstance browser;
    protected WebDriver webDriver = null;
    protected Integer implicitWaitSeconds = 0;

    /**
     * Constructor that set Implicit Wait
     * @param browser
     * @throws Exception
     */
    public Page(BrowserInstance browser) throws Exception  {
        this.browser = browser;
        this.webDriver = browser.getInstanceDriver();

        //init implicit wait time
        implicitWaitSeconds = Integer.parseInt(ConfigurationSettings.getInstance().getSetting("implicit.wait.seconds"));
        browser.setImplicitWait(implicitWaitSeconds);

        url = "";
        pageTitle = "";
    }

    /**
     * Go to default URL server
     */
    public void gotoUrl(String url){
        this.url = url;
        //Logger.addLog("Page.Goto url:" + url);
        this.browser.gotoUrl(url);
        //Logger.addLog("Browser.goto url:" + url);
    }

    public abstract void verifyPage() throws Exception;
}
